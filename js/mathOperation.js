console.log("hello ready for math ?")

var number1, number2, operator, result;
var session = 1;
var questionNumber = 1;
var userScore = 0;

//-- Get the settings value
var params= window.location.href.split('?');
var data = params[1].split('&');
console.log(data);

//-- Set the min and max value deppending the settings
var min, max;
switch ( data[1] ){
	case ("level1"):
		min = 0;
		max = 10;
		break;
	case ("level2"):
		min = 3;
		max = 15;
		break;
	case ("level3"):
		min = 10;
		max = 100;
		break;
	case ("level4"):
		min = 100;
		max = 1000;
		break;
	default:
		min = 0;
		max = 10;
		break;
}

newOperation();

function newOperation(){
	number1 = Math.floor((Math.random() * max) + min);
	number2 = Math.floor((Math.random() * max) + min);
	switch (data[0]){
		case ("add"):
			result = number1 + number2;
			operator = '+';
			break;
		case ("sub"):
			if(number2 > number1){
				[number1, number2] = [number2, number1];
			}
			result = number1 - number2;
			operator = '-';
			break;
		case ("mul"):
			result = number1 * number2;
			operator = 'X';
			break;
		case ("div"):
			result = number1 / number2;
			operator = '/';
			break;
	}
}

//-- Print in the console the operation and result. Don't forget to comment for production
console.log("number1 : " + number1);
console.log("number2 : " + number2);
console.log("result : "  + result );

operation  = document.getElementById("operation");
operation.textContent = number1 + ' ' + operator + ' ' + number2 + " = ";

//-- Listener on the VALID button
var valid = document.getElementById("validResult");
valid.addEventListener("click", function (userResult) {
	valid.style.visibility = "hidden";
	next.style.visibility = "visible";
	var userResult = Number(document.getElementById("userResult").value);
	if(userResult === result){
		//console.log("Your answer is good !");
		userScore += 1;
		document.getElementById("resultText").textContent = "Your answer is good !";
	} else {
		//console.log("Your answer is bad !");
		document.getElementById("resultText").textContent = "Your answer is bad ! the good answer was " + result + " !" ;
	}
	document.getElementById("nextOperation").focus();
	document.getElementById("userScore").textContent = userScore;
});

//-- Listener on the NEXT button
var next = document.getElementById("nextOperation");
next.style.visibility = "hidden";
next.addEventListener("click", function(){
	questionNumber += 1;
	if(questionNumber > 10){
		questionNumber = 10;
		document.getElementById("score").style.visibility = "visible";
	}

	initOperation();
	valid.style.visibility = "visible";
	next.style.visibility = "hidden";
	//document.getElementById("questionNumber").textContent = questionNumber;
	document.getElementById("userResult").focus();

});

//-- Listener on the CONTINUE button
var continueSession = document.getElementById("continue");
continueSession.addEventListener("click", function(){
	questionNumber = 1;
	session += 1;
	userScore = 0;
	questionNumber = 1;
	document.getElementById("score").style.visibility = "hidden";
	document.getElementById("sessionNumber").textContent = session;
	initOperation();
});

function initOperation(){
	newOperation();
	console.log("number1 : " + number1);
	console.log("number2 : " + number2);
	console.log("result : "  + result );
	operation  = document.getElementById("operation");
	operation.textContent = number1 + ' ' + operator + ' ' + number2 + " = ";
	document.getElementById("userResult").value = "";
	document.getElementById("resultText").textContent = "";
	document.getElementById("questionNumber").textContent = questionNumber;
}


