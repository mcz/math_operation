var start = document.getElementById("start");
var operation = document.getElementById("operation");

start.addEventListener("click", function(){
	var operation  = document.getElementById("operation").value;
	var difficulty = document.getElementById("difficulty").value;
	var timer      = document.getElementById("timer").value;
	window.location.href = 'mathOperation.html?' + operation + '&' + difficulty + '&' + timer; 
});

operation.addEventListener("change", function(e){
	var sigle;
	var getSigle = document.getElementById("operation").value
	if (getSigle === "add"){ 
		sigle = "+";
	} else if (getSigle === "sub") {
		sigle = "-";

	} else if (getSigle === "mul") {
		sigle = "X";
	} else {
		sigle = "/";
	}
	
	document.getElementById("operationSigle").textContent = sigle;
});

